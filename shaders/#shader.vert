#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject {
	mat4 model;			//converts things to world space -> places meshes in world
	mat4 view;			//transform to camera space
	mat4 proj;			//transform to 2d space for projection
} ubo;


layout(location = 0) in vec3	inPosition;
layout(location = 1) in vec3	inColor;
layout(location = 2) in vec2	inTexCoord;
layout(location = 3) in vec3	inNormal;

layout(location = 0) out vec3   fragColor;
layout(location = 1) out vec2   fragTexCoord;
layout(location = 2) out float  cosThetaLight;

out gl_PerVertex {
    vec4 gl_Position;
};



void main() {
	vec3 vertPos_camSpace = ( ubo.view * ubo.model * vec4(inPosition, 1.0) ).xyz;
	// vec3 eyeDir_camSpace = vec3(0,0,0) - vertPos_camSpace; // camera is at (0,0,0)

	// vec3 lightPos_wrldspace = vec3(10, 10, 10);
	// vec3 lightDir_camspace = ( ubo.view * vec4(lightpos_wrldspace, 1)).xyz;					// Normal of the computed fragment, in camera space
	// vec3 lightDrection_camspace = normalize(lightdir_camspace + eyedir_camspace);	   // Direction of the light (from the fragment to the light
    // vec3 norm_camspace = normalize(( ubo.view * ubo.model * vec4(inNormal,0)).xyz);

    // gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
	gl_Position = ubo.proj * vec4(vertPos_camSpace, 1.0); // THE ORDER HERE IS F IMPORTANT FOR F SAKE AAARRRRHHHHHHHHHH
	fragColor = inColor;
	fragTexCoord = inTexCoord;
	// float length = length(lightDir_camSpace);
	// cosThetaLight = clamp( dot( norm_camspace, LightDirection_cameraspace ), 0,1 ) / sqrt(sqrt(length));
	// distance = 1;
	cosThetaLight = 0.5;			   // Cosine of the angle between the normal and the light direction, clamped
}
