#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3	fragColor;
layout(location = 1) in vec2	fragTexCoord;
layout(location = 2) in float	cosThetaLight;

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) out vec4 outColor;


void main() {

    vec3 MaterialDiffuseColor = texture(texSampler, fragTexCoord).rgb;
    vec3 LightColor = vec3(1.0, 1.0, 0.94);
    float LightPower = 1.;

    outColor = vec4( (MaterialDiffuseColor * LightColor * LightPower * cosThetaLight), 1 );
}
