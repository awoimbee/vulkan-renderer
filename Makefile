NAME	=	vlk_test

CC = g++

# CFLAGS	=	-Ofast -ffast-math -flto=full -msse

SRC_NAME =	main.cpp							\


SRC_FOLDERS =
################################################################################

OBJ_NAME = $(SRC_NAME:.cpp=.o)

SRC_PATH =	src
OBJ_PATH =	obj

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

LDLIBS = `pkg-config --static --libs glfw3 vulkan` -lpthread -ltinyobjloader

LDFLAGS = -L ./libs/tinyobjloader/

CFLAGS += -std=c++17 -O3 -I./libs -I./src -I./libs/tinyobjloader -I./inc

################################################################################

all : $(NAME)

TOBJLOADR:
	cmake libs/tinyobjloader/CMakeLists.txt
	make -s -C libs/tinyobjloader

$(NAME): TOBJLOADR $(OBJ)
	g++ $(CFLAGS) $(LDFLAGS) $(LDLIBS) $(OBJ) -o $(NAME)

$(OBJ_PATH) :
	@mkdir -p $(OBJ_PATH) 2> /dev/null
	# @mkdir -p $(addprefix $(OBJ_PATH)/,$(SRC_FOLDERS)) 2> /dev/null
	@printf "$(GRN)Building with \"$(CFLAGS)\":$(EOC)\n"

$(OBJ_PATH)/%.o : $(SRC_PATH)/%.cpp | $(OBJ_PATH)
	@printf "\t$(CC) (...) $@\n"
	@g++ $(CFLAGS) -o $@ -c $<

libclean :
	@printf "$(YLW)Cleaning SDL2...$(EOC)\n"
	# @rm -rf SDL2/build SDL2/lib SDL2/share SDL2/bin SDL2/include
	@printf "$(YLW)Cleaning libft...$(EOC)\n"
	# @make -s fclean -C libft

objclean :
	@rm -rf $(OBJ_PATH)
	@printf "$(RED)$(OBJ_PATH) removed$(EOC)\n"

outclean :
	@rm -f $(NAME)
	@printf "$(RED)$(NAME) removed$(EOC)\n"

clean	:	libclean	objclean
fclean	:	clean		outclean
re		:	fclean		all
sfclean	:	objclean	outclean
sre		:	sfclean		$(NAME)

.PHONY: all libclean objclean clean re fclean sfclean sre

RED = \033[1;31m
GRN = \033[1;32m
YLW = \033[1;33m
INV = \033[7m
EOC = \033[0m
