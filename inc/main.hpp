/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/25 02:04:33 by awoimbee          #+#    #+#             */
/*   Updated: 2019/05/25 18:06:11 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VLKT_MAIN_H
# define VLKT_MAIN_H
# define GLFW_INCLUDE_VULKAN
# include "g_libs.hpp"
# include "datatypes.hpp"


//windows is dumb and introduce weird thing so i have to undef // ????
#undef min
#undef max

# ifndef _DEBUG
#  define NDEBUG
# endif
# ifdef NDEBUG
#  define enableValidationLayers false
# else
#  define enableValidationLayers true
# endif

/* We only load 1 model and 1 texture, both are hardcoded right now */
# define MODEL_PATH "models/volcano 02_subdiv_01.obj"
# define TEXTURE_PATH "models/maps/volcano 02 diff.jpg"

extern const int WIDTH;
extern const int HEIGHT;
extern const int MAX_FRAMES_IN_FLIGHT;
extern const std::vector<const char*> validationLayers; // debug layers
extern std::vector<const char*> enabledLayers;
extern const std::vector<const char*> deviceExtensions; // extensions needed by the renderer

/* vertex hash function for unordered map */
namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

#endif
