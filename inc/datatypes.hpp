/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   datatypes.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/25 01:49:28 by awoimbee          #+#    #+#             */
/*   Updated: 2019/05/25 02:37:12 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VLKT_DATATYPES_H
# define VLKT_DATATYPES_H
# include "g_libs.hpp"
# include <vector>

/*
Contain the indices of all needed queue families
	it's in coordinations with "findQueueFamilies(VkPhysicalDevice)" "createLogicalDevice()" -> impact vertex bufferf
*/
struct QueueFamilyIndices {
	int graphicsFamily =  -1; //draw
	int presentFamily =   -1; //show on screen
	int transfertFamily = -1; //transfert from RAM to DRAM, from CPU to GPU

	bool isComplete() {
		return (graphicsFamily >= 0
			&& presentFamily >= 0
			&& transfertFamily >= 0);
	}
};

// Contains : Basic surface capabilities(min/max number of images in swap chain, min/max width & height of images), Surface formats(pixel format, color space), Available presentation modes
struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR           capabilities;
	std::vector<VkSurfaceFormatKHR>    formats;
	std::vector<VkPresentModeKHR>      presentModes;
};

//represents a vertex..
struct Vertex {
	glm::vec3    pos;
	glm::vec3    color;
	glm::vec3    norm;
	glm::vec2    texCoord;

	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {
			.binding = 0,
			.stride = sizeof(Vertex),
			.inputRate = VK_VERTEX_INPUT_RATE_VERTEX //change for instanced rendering
		};
		return (bindingDescription);
	};

	static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 4> attributeDescriptions = {{
			{.location = 0, .binding = 0, .format = VK_FORMAT_R32G32B32_SFLOAT, .offset = offsetof(Vertex, pos)},
			{.location = 1, .binding = 0, .format = VK_FORMAT_R32G32B32_SFLOAT, .offset = offsetof(Vertex, color)},
			{.location = 2, .binding = 0, .format = VK_FORMAT_R32G32_SFLOAT,    .offset = offsetof(Vertex, texCoord)},
			{.location = 3, .binding = 0, .format = VK_FORMAT_R32G32B32_SFLOAT, .offset = offsetof(Vertex, norm)}
		}};
		return (attributeDescriptions);
	};

	bool operator==(const Vertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord;
	};
};

//Uniform buffer, to pass info to vertex shader
struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
};

class Camera {
public:
	glm::vec3 pos = { 2.0f, 2.0f, 2.0f };
	glm::vec3 rot = { 0.f, 0.f,0.f };
	glm::mat4 viewMatrix;
};

#endif
