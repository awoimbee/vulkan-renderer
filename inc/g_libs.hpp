/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   g_libs.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awoimbee <awoimbee@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/25 02:27:06 by awoimbee          #+#    #+#             */
/*   Updated: 2019/05/25 02:28:12 by awoimbee         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VLKT_GLIBS_H
# define VLKT_GLIBS_H

# define GLFW_INCLUDE_VULKAN
# include <GLFW/glfw3.h>

# include <iostream>
# include <stdexcept>
# include <functional>
# include <cstdlib>
# include <vector>
# include <map>
# include <set>
# include <algorithm>
# include <fstream>
# include <glm/glm.hpp>
# include <array>
# include <chrono>
# include <unordered_map>
# include <thread>

# define GLM_FORCE_RADIANS
# define GLM_FORCE_DEPTH_ZERO_TO_ONE //because we are using Vulkan, no OpenGL
# define GLM_ENABLE_EXPERIMENTAL
# include <glm/glm.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtx/quaternion.hpp>
# include <glm/gtx/transform.hpp>
# include <glm/gtx/hash.hpp>

# define STB_IMAGE_IMPLEMENTATION
# include "stb_image.h"

# define TINYOBJLOADER_IMPLEMENTATION
# include "tiny_obj_loader.h"

#endif
